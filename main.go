package main

import (
	"plangbot/config"
	"plangbot/ep_cli"
	"plangbot/ep_help"
	"plangbot/ep_tg"
	"plangbot/executor"
	"plangbot/proto"
	"plangbot/ui"

	"github.com/asvedr/gotgbot/v2"
	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var Version = "N/A"

var Config = di.Singleton[*config.Config]{
	ErrFunc: cl.BuildConfig[config.Config],
}

var Executor = di.Singleton[proto.Executor]{
	PureFunc: func() proto.Executor {
		return executor.New(Config.Get())
	},
}

var Bot = di.Singleton[*gotgbot.Bot]{
	PureFunc: func() *gotgbot.Bot {
		token := Config.Get().Token
		return gotgbot.NewBotNoConnect(token, nil)
	},
}

var UI = di.Singleton[proto.Process]{
	PureFunc: func() proto.Process {
		return ui.New(Bot.Get(), Executor.Get())
	},
}

func main() {
	cl.NewRunnerMulty(
		"",
		"robot plang (version: "+Version+")",
		ep_cli.EP{Executor: Executor.Get},
		ep_help.EP{Executor: Executor.Get},
		ep_tg.EP{UI: UI.Get},
	).Run()
}
