package executor

import (
	"fmt"
	"plangbot/config"
	"plangbot/proto"
	"strings"

	"gitlab.com/asvedr/plang"
)

type executor struct {
	config *config.Config
}

func New(config *config.Config) proto.Executor {
	return executor{config: config}
}

func (self executor) Execute(code string) string {
	for _, sym := range []string{"’", "‘"} {
		code = strings.ReplaceAll(code, sym, "'")
	}
	if !has_dot_in_end(code) {
		code += "."
	}
	program, err := plang.Compile(code)
	if err != nil {
		return "ERR: " + err.Error()
	}
	res, err := program.Execute(
		self.config.Threads,
		self.config.Samples,
		self.config.MaxStack,
	)
	if err != nil {
		return "ERR: " + err.Error()
	}
	lines := []string{}
	for key, val := range res {
		lines = append(lines, fmt.Sprintf("%s: %v", key, val))
	}
	return strings.Join(lines, "\n")
}

func (self executor) DocList() string {
	return plang.HelpFunList()
}

func (self executor) DocFun(fun string) string {
	return plang.HelpFun(fun)
}

func has_dot_in_end(code string) bool {
	runes := []rune(strings.TrimSpace(code))
	if len(runes) == 0 {
		return false
	}
	return runes[len(runes)-1] == '.'
}
