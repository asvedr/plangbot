package ep_help

import (
	"plangbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	Executor func() proto.Executor
}

type request struct {
	Fun string `opt:"t"`
}

func (self EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "help",
		Description: "get fun list of fun doc",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self EP) Execute(prog string, args any) error {
	fun := args.(request).Fun
	exec := self.Executor()
	if fun == "" {
		println(exec.DocList())
	} else {
		println(exec.DocFun(fun))
	}
	return nil
}
