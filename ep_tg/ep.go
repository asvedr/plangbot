package ep_tg

import (
	"plangbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	UI func() proto.Process
}

func (self EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "tg",
		Description: "run tg bot",
	}
}

func (self EP) Execute(prog string, args any) error {
	self.UI().Run()
	return nil
}
