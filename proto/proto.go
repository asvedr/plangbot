package proto

type Process interface {
	Run()
}

type Executor interface {
	Execute(code string) string
	DocList() string
	DocFun(fun string) string
}
