package config

type Config struct {
	Token    string   `default:""`
	Threads  int      `default:"2"`
	Samples  int      `default:"10000"`
	MaxStack int      `default:"1000"`
	Meta     struct{} `prefix:"plangbot_"`
}
