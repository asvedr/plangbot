package ep_cli

import (
	"os"
	"plangbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	Executor func() proto.Executor
}

type request struct{ Path string }

func (self EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "exe",
		Description: "execute file script",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self EP) Execute(prog string, args any) error {
	path := args.(request).Path
	bts, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	txt := string(bts)
	resp := self.Executor().Execute(txt)
	if resp == "" {
		resp = "<empty>"
	}
	println(resp)
	return nil
}
