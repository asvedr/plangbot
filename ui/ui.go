package ui

import (
	"errors"
	"log"
	"plangbot/proto"
	"strings"
	"time"

	"github.com/asvedr/gotgbot/v2"
	"github.com/asvedr/gotgbot/v2/ext"
	"github.com/asvedr/gotgbot/v2/ext/handlers"
	"github.com/asvedr/gotgbot/v2/ext/handlers/filters/message"
)

type ui struct {
	bot      *gotgbot.Bot
	executor proto.Executor
}

func New(
	bot *gotgbot.Bot,
	executor proto.Executor,
) proto.Process {
	return &ui{bot: bot, executor: executor}
}

func (self *ui) Run() {
	for {
		err := self.bot.Reconnect(nil)
		if err == nil {
			err = self.main_loop()
		}
		log.Printf("TgUi error: %v", err)
		time.Sleep(time.Second)
	}
}

func (self *ui) main_loop() error {
	bot := self.bot
	dispatcher := ext.NewDispatcher(&ext.DispatcherOpts{
		// If an error is returned by a handler, log it and continue going.
		Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
			log.Println("an error occurred while handling update:", err.Error())
			return ext.DispatcherActionNoop
		},
		MaxRoutines: ext.DefaultMaxRoutines,
	})
	updater := ext.NewUpdater(dispatcher, nil)

	handler := func(bot *gotgbot.Bot, ctx *ext.Context) error {
		return self.react(ctx)
	}

	// Add echo handler to reply to all text messages.
	dispatcher.AddHandler(
		handlers.NewMessage(message.Text, handler),
	)
	err := updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: false,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		log.Printf("failed to start polling: %v", err.Error())
		return err
	}
	log.Printf("%s has been started...\n", bot.User.Username)

	// Idle, to keep updates coming in, and avoid bot stopping.
	updater.Idle()
	return errors.New("stop idling")
}

func (self *ui) react(ctx *ext.Context) error {
	msg := ctx.EffectiveMessage.Text
	msg = strings.ToLower(strings.TrimSpace(msg))
	msg = strings.TrimSpace(msg)
	var txt string
	if strings.HasPrefix(msg, "/help") {
		txt = self.help_resp(strings.TrimPrefix(msg, "/help"))
	} else {
		txt = self.executor.Execute(msg)
	}
	if txt == "" {
		txt = "<empty>"
	}
	_, err := ctx.EffectiveMessage.Reply(self.bot, txt, nil)
	return err
}

func (self *ui) help_resp(msg string) string {
	msg = strings.TrimSpace(msg)
	if msg == "" {
		return self.executor.DocList()
	}
	return self.executor.DocFun(msg)
}
