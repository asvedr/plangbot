module plangbot

go 1.21.3

require (
	github.com/asvedr/gotgbot/v2 v2.0.1
	gitlab.com/asvedr/cldi v0.6.0
	gitlab.com/asvedr/plang v0.13.0
)

require gitlab.com/asvedr/gstack v1.0.1 // indirect
